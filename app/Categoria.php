<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table ='categorias';
    protected $primaryKey = 'id';

    public function lists(){
        return $this->hasMany('App\Funcion', 'categorias_id');
    }

}
