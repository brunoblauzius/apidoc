<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcion extends Model
{
    protected $table ='functions';
    protected $primaryKey = 'id';

    public function lists(){
        return $this->belongsTo('App\Categoria', 'categorias_id');
    }

}
