<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categoria;

class CategoriaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function cadastro(){
        return view('categorias.cadastro');
    }

    public function editar(Request $request, $id = null){
        return view('categorias.editar')->with('registro', Categoria::find(intval($id)));
    }


    public function index(){
        return view('categorias.index')->with('registros', Categoria::all());
    }


    public function add(Request $request){

        $model = new Categoria();
        $model->nome  = (trim($request->input('nome')));
        $model->save();

        return redirect('/admin/categorias');

    }

    public function edit(Request $request){

        $model = Categoria::find(intval($request->input('id')));
        $model->nome  = (trim($request->input('nome')));
        $model->save();

        return redirect('/admin/categorias');

    }


}
