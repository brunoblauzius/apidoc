<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Funcion;
use App\Utils\Utils;
use Illuminate\Http\Request;

class FunctionsController extends Controller
{

    private $metodos = [ 'POST', 'GET', 'PUT', 'DELETE' ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index (Request $request){
        $registros = Funcion::all();
        return view('functions.index')
            ->with('registros', $registros)
            ->with('categorias', Categoria::all());
    }

    public function cadastro (Request $request){
        return view('functions.cadastro')
            ->with('categorias', Categoria::all())
            ->with('metodos', $this->metodos);
    }

    public function editar (Request $request, $metodo){
        $registro = Funcion::where('titulo', $metodo)->first();
        return view('functions.editar')
            ->with('registro', $registro)
            ->with('categorias', Categoria::all())
            ->with('metodos', $this->metodos);
    }


    public function add (Request $request){
        try{

            $model = new Funcion();

            $model->categorias_id = $request->input('categorias_id');
            $model->titulo = $request->input('titulo');
            $model->metodo = $request->input('metodo');
            $model->link = $request->input('link');
            $model->input = Utils::htmlEntityEncode($request->input('input'));
            $model->output = Utils::htmlEntityEncode($request->input('output'));

            $model->save();


            return redirect('admin/functions/cadastro');

        } catch ( \Exception $ex ){
            echo $ex->getMessage();
        }
    }

    public function edit (Request $request){
        try{

            $model = Funcion::find($request->input('id'));
            $model->categorias_id = $request->input('categorias_id');
            $model->titulo = $request->input('titulo');
            $model->metodo = $request->input('metodo');
            $model->link = $request->input('link');
            $model->input = Utils::htmlEntityEncode($request->input('input'));
            $model->output = Utils::htmlEntityEncode($request->input('output'));
            $model->save();

            return redirect('admin/functions/');

        } catch ( \Exception $ex ){

        }
    }

}
