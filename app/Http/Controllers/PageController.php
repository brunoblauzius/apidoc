<?php

namespace App\Http\Controllers;

use App\Funcion;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function home(Request $request){
        return view('page.home')->with('menu', $this->getMenu());
    }

    public function login(Request $request){
        return view('page.login');
    }


    public function perfil(Request $request, $metodo = null){

        $registro = Funcion::where('titulo', $metodo)->first();

        return view('page.perfil')
            ->with('menu', $this->getMenu())
            ->with('registro', $registro);
    }


}
