<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cadastro(){
        return view('auth.register');
    }

    public function index(){
        return view('user.index')->with('registros', User::all());
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function editar(Request $request, $id = null){
        return view('user.editar')->with('registro', User::find(intval($id)));
    }


}
