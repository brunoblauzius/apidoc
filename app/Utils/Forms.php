<?php

namespace App\Utils;

class Forms{
    
    
    public static function listGrid( $parameters ){
        if( !is_array($parameters) && !empty($parameters)){
            echo '<li><a href="#">'.$parameters.'<i class="icon-cross2"></i></a></li>';
        } else {
            foreach ($parameters as $key => $parameter){
                if(!empty($parameter) && !in_array($key, ['page', 'list_view'])){
                    self::listGrid($parameter);
                }
            }
        }
    }
    
    public static function rangerPik( $parameters ){
        return explode(',', $parameters);
    }
    
    
    
}

