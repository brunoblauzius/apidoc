<?php


namespace App\Utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResponseMSG
 *
 * @author BRUNO
 */
class ResponseMSG {
    //put your code here
    
    public final static function responseErrors( $erros ){
        $response = array();        
        foreach ( $erros->getMessages() as $key => $value){
            $response[$key] = ($value[0]);
        }
        return $response;
    } 
    
    public static final function responseHtml( $erros ){
        $response = array();        
        foreach ( $erros->getMessages() as $key => $value){
            $response[] = '<b>' . ($value[0]) . '</b>';
        }
        return join('<br>', $response);
    }
    
    
}
