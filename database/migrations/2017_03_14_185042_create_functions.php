<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('functions', function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->integer('categorias_id')->unsigned();
            $table->string('titulo', 200);
            $table->string('link', 255);
            $table->string('metodo', 20)->default('get');
            $table->text('input');
            $table->text('output');
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
