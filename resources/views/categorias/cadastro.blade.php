@extends('layout.painel')

@section('conteudo')

   <div class="row" style="min-height: 380px;">
       <div class="col m12">
           <form action="{{url('admin/categorias/add')}}" method="post">
               <?= csrf_field()?>
               <div class="row">
                   <h3>Formulário de Cadastro</h3>
                   <div class="col m8">
                       <small>Categoria:</small><br>
                       <input name="nome" placeholder="Nome da Categoria" value="{{old('nome')}}"/>
                   </div>
                   <div class="col m8">
                       <button class="btn btn-primary">Cadastrar</button>
                   </div>
               </div>

           </form>
       </div>
   </div>

    <script>
       $(document).ready(function() {
           $('select').material_select();
       });
   </script>

@endsection

