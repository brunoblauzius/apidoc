@extends('layout.painel')

@section('conteudo')

    <div class="row" style="min-height: 380px;">
        <div class="col m12">
            <form action="{{url('admin/categorias/edit')}}" method="post">
                <?= csrf_field()?>
                <input name="id" type="hidden" value="{{$registro->id}}">
                <div class="row">
                    <h3>Formulário de Cadastro</h3>
                    <div class="col m8">
                        <small>Categoria:</small><br>
                        <input name="nome" placeholder="Nome da Categoria" value="{{$registro->nome}}"/>
                    </div>
                    <div class="col m8">
                        <button class="btn btn-primary">Alterar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>

@endsection
