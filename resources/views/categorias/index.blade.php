@extends('layout.painel')

@section('conteudo')

    <div class="row" style="margin: 20px 0px;">
        <a href="{{url('/admin/categorias/cadastro')}}" class="btn waves-effect waves-light red">Cadastar</a>
    </div>

    <div class="row">
        <table>
            <thead>
            <th>Categoria</th>
            <th width="10%">Ação</th>
            </thead>
            <tbody>

            @foreach( $registros as $registro )
                <tr>
                    <td>{{$registro->nome}}</td>
                    <td>
                        <a href="{{url('/admin/categorias/editar')}}/{{$registro->id}}" class="btn btn-sm re">editar</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


@endsection