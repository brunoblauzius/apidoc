@extends('layout.painel')

@section('conteudo')

    <script>
        tinymce.init({
            selector: '.textarea'
        });
    </script>


   <form action="{{url('admin/functions/add')}}" method="post">
        <?= csrf_field()?>
       <div class="row">
           <h3>Formulário de Cadastro</h3>
           <div class="col m8">
               <small>Sessão:</small><br>
               <select class="icons" name="categorias_id">
                   @foreach( $categorias as $categoria )
                       <option value="{{$categoria->id}}"> {{$categoria->nome}} </option>
                   @endforeach
               </select>
           </div>
           <div class="col m8">
               <small>Titulo:</small><br>
               <input name="titulo" placeholder="Titulo" value="{{old('titulo')}}"/>
           </div>
           <div class="col m8">
               <small>Metodo:</small><br>
               <select name="metodo">
                   @foreach( $metodos as $metodo)
                       <?php
                           $selected = null;
                           if(old('metodo') == $metodo){
                               $selected = 'selected';
                           }
                       ?>
                       <option value="{{$metodo}}" {{$selected}}> {{$metodo}} </option>
                   @endforeach
               </select>
           </div>
           <div class="col m8">
               <small>URL de Acesso:</small><br>
               <input name="link" placeholder="Url do metodo" value="{{old('link')}}"/>
           </div>

           <div class="col m8">
               <small>Parametros de entrada INPUT:</small><br>
               <textarea name="input" class="textarea" placeholder="parametros de entrada">{{old('input')}}</textarea>
           </div>

           <div class="col m8">
               <small>Parametros de saída OUTPUT:</small><br>
               <textarea name="output" class="textarea" placeholder="parametros de saída">{{old('output')}}</textarea>
           </div>

           <div class="col m8">
               <button class="btn btn-primary">Cadastrar</button>
           </div>

       </div>

   </form>


   <script>
       $(document).ready(function() {
           $('select').material_select();
       });
   </script>

@endsection

