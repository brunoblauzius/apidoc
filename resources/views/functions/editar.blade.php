@extends('layout.painel')

@section('conteudo')

    <script>
        tinymce.init({
            selector: '.textarea'
        });
    </script>


    <form action="{{url('admin/functions/edit')}}" method="post">
        <?= csrf_field()?>
        <input type="hidden" name="id" value="{{$registro->id}}">
        <div class="row">
            <h3>Formulário de Edição</h3>
            <div class="col m8">
                <small>Sessão:</small><br>
                <select class="icons" name="categorias_id">
                    @foreach( $categorias as $categoria )
                        @if( $registro->categorias_id == $categoria->id)
                            <option value="{{$categoria->id}}" selected="selected"> {{$categoria->nome}} </option>
                        @else
                            <option value="{{$categoria->id}}"> {{$categoria->nome}} </option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col m8">
                <small>Titulo:</small><br>
                <input name="titulo" placeholder="Titulo" value="{{$registro->titulo}}"/>
            </div>
            <div class="col m8">
                <small>Metodo:</small><br>
                <select name="metodo">
                    @foreach( $metodos as $metodo)
                        <?php
                        $selected = null;
                        if($registro->metodo == $metodo){
                            $selected = 'selected';
                        }
                        ?>
                        <option value="{{$metodo}}" {{$selected}}> {{$metodo}} </option>
                    @endforeach
                </select>
            </div>
            <div class="col m8">
                <small>URL de Acesso:</small><br>
                <input name="link" placeholder="Url do metodo" value="{{$registro->link}}"/>
            </div>

            <div class="col m8">
                <small>Parametros de entrada INPUT:</small><br>
                <textarea name="input" class="textarea" placeholder="parametros de entrada">{{$registro->input}}</textarea>
            </div>

            <div class="col m8">
                <small>Parametros de saída OUTPUT:</small><br>
                <textarea name="output" class="textarea" placeholder="parametros de saída">{{$registro->output}}</textarea>
            </div>

            <div class="col m8">
                <button class="btn btn-primary">Cadastrar</button>
            </div>

        </div>

    </form>


    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>

@endsection

