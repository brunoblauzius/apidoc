<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link href="{{url('public/materialize/css/materialize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('public/css/custom.css')}}" rel="stylesheet" type="text/css">

    <!-- js-->
    <script src="{{url('public/js/jquery-3.0.0.min.js')}}"?<?=time();?>></script>
    <script src="{{url('public/materialize/js/materialize.js')}}"?<?=time();?>></script>

</head>
<body>

<script>
    $( document ).ready(function(){
        $(".button-collapse").sideNav();
    });
</script>

<div class="container">
    <div class="row">
        <div class="col m12" style="min-height: 410px;">
            @yield('conteudo')
        </div>
    </div>
</div>


<footer class="page-footer red darken-4">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">API DOC</h5>
                <p class="grey-text text-lighten-4">Documentação da api Lojacorr LTDA.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">Lojacorr</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Broker One</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">YesOK</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © {{date('Y')}} todos os direitos reservados a codewave
            <a class="grey-text text-lighten-4 right" href="#!">veja mais</a>
        </div>
    </div>
</footer>



</body>
</html>