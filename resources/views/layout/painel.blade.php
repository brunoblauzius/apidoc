<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link href="{{url('public/materialize/css/materialize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('public/css/custom.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- js-->
    <script src="{{url('public/js/jquery-3.0.0.min.js')}}"?<?=time();?>></script>
    <script src="{{url('public/materialize/js/materialize.js')}}"?<?=time();?>></script>
    <script src="{{url('public/js/tinymce/tinymce.min.js')}}"?<?=time();?>></script>

</head>
<body>

<script>
    $( document ).ready(function(){
        $(".button-collapse").sideNav();
    });
</script>


<nav>
    <div class="nav-wrapper red darken-4">
        <a href="{{url('/')}}" class="brand-logo"><img src="{{asset('public/image/Rede-Lojacorr.png')}}" height="64"></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{url('/admin/functions')}}">Funções</a></li>
            <li><a href="{{url('/admin/categorias')}}">Categorias</a></li>
            <li><a href="{{url('/admin/usuarios')}}">Usuarios</a></li>
            <li><a href="{{ route('logout') }}"><i class="material-icons">power_settings_new</i></a></li>
        </ul>
    </div>
</nav>

<div class="">
    <div class="container">
        <div class="col m12">
            @yield('conteudo')
        </div>
    </div>
</div>



</body>
</html>