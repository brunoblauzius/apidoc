<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

    <link href="{{url('public/materialize/css/materialize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('public/css/custom.css')}}" rel="stylesheet" type="text/css">


    <!-- js-->
    <script src="{{url('public/js/jquery-3.0.0.min.js')}}"?<?=time();?>></script>
    <script src="{{url('public/materialize/js/materialize.js')}}"?<?=time();?>></script>
    <script src="{{url('public/js/tinymce/tinymce.min.js')}}"?<?=time();?>></script>

</head>
<body>

    <script>
        $( document ).ready(function(){
            $(".button-collapse").sideNav();
        });
    </script>


    <nav>
        <div class="nav-wrapper red darken-4">
            <a href="{{url('/')}}" class="brand-logo"><img src="{{asset('public/image/Rede-Lojacorr.png')}}" height="64"></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{url('/login')}}">Login</a></li>
            </ul>
        </div>
    </nav>

    <div class="">
        <div class="row">
            <div class="col m3">
                <div class="collection">
                    @foreach( $menu as $m)
                        <a href="#!" class="collection-item">{{$m->nome}}</a>
                        @foreach( $m->lists as $fun)
                            <a href="{{url('/perfil')}}/{{$fun->titulo}}" class="collection-item" style="color: #0d47a1 !important;">{{$fun->titulo}}</a>
                        @endforeach
                    @endforeach

                </div>
            </div>

            <div class="col m9">
                @yield('conteudo')
            </div>

        </div>
    </div>

    <footer class="page-footer red darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">API DOC</h5>
                    <p class="grey-text text-lighten-4">Documentação da api Lojacorr LTDA.</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Lojacorr</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Broker One</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">YesOK</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © {{date('Y')}} todos os direitos reservados a codewave
                <a class="grey-text text-lighten-4 right" href="#!">veja mais</a>
            </div>
        </div>
    </footer>


</body>
</html>