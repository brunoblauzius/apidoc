@extends('layout.blank')

@section('conteudo')

    <div class="col m8 offset-m2" style="margin-top: 60px; margin-bottom: 30px;">
        <div class="col m3 offset-m4">
            <a href="{{url('/')}}" class="brand-logo"><img src="{{asset('public/image/Rede-Lojacorr.png')}}" height="64"></a>
        </div>
    </div>

    <div class="row">
        <form action="{{url('admin/logar')}}" method="post" class="col m8 offset-m2">
            <div class="row">
                <div class="col s6 offset-m3">
                    <input id="username" type="text" class="validate" placeholder="Username:">
                </div>
            </div>
            <div class="row">
                <div class="col s6 offset-m3">
                    <input id="password" type="password" class="validate" placeholder="Senha:">
                </div>
            </div>
            <div class="row">
                <div class="input-field col m6 offset-m3">
                    <button class="btn waves-effect  red darken-4 waves-light" style="width: 100% !important;"> Logar </button>
                </div>
            </div>
        </form>
    </div>

@endsection