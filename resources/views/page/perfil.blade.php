@extends('layout.site')

@inject('util', 'App\Utils\Utils')

@section('conteudo')

    <h3><small>Categoria:</small> {{$registro->lists->nome}} </h3>

    <div class="blue-grey lighten-5 request">
        <h5 class="title-label-{{strtolower($registro->metodo)}}"><strong class="title-type-{{strtolower($registro->metodo)}}">{{strtoupper($registro->metodo)}}:</strong> {{$registro->titulo}}</h5>
        <p>
            <span>URL</span>: <strong class="text-{{strtolower($registro->metodo)}}">{{$registro->link}}</strong>
        </p>
        <p>
        <strong>Request:</strong>
            <blockquote>
                <?= $util->htmlEntityDecode($registro->input) ?>
            </blockquote>
        </p>
    </div>

    <div class="grey lighten-5 request" style="margin-top: 10px;">
        <h5>Response</h5>
        <blockquote>
            <?= $util->htmlEntityDecode($registro->output) ?>
        </blockquote>
    </div>

@endsection

