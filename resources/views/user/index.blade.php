@extends('layout.painel')

@section('conteudo')

    <div class="row" style="margin: 20px 0px;">
        <a href="{{url('/admin/usuarios/cadastro')}}" class="btn waves-effect waves-light red">Cadastar</a>
    </div>

    <div class="row">
        <table>
            <thead>
            <th>Nome</th>
            <th>E-mail</th>
            <th width="10%">Ação</th>
            </thead>
            <tbody>

            @foreach( $registros as $registro )
                <tr>
                    <td>{{$registro->name}}</td>
                    <td>{{$registro->email}}</td>
                    <td>
                        <a href="{{url('/admin/usuarios/editar')}}/{{$registro->id}}" class="btn btn-sm re">Editar</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


@endsection