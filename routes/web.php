<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('/login', 'PageController@login');
Route::get('/painelo', 'PageController@login');
Route::get('/perfil/{metodo}', 'PageController@perfil');


Route::get('/admin/categorias/cadastro', 'CategoriaController@cadastro');
Route::post('/admin/categorias/add', 'CategoriaController@add');
Route::get('/admin/categorias/', 'CategoriaController@index');
Route::get('/admin/categorias/editar/{id}', 'CategoriaController@editar');
Route::post('/admin/categorias/edit', 'CategoriaController@edit');

Route::get('/admin/functions/', 'FunctionsController@index');
Route::get('/admin/functions/cadastro', 'FunctionsController@cadastro');
Route::get('/admin/functions/editar/{metodo}', 'FunctionsController@editar');
Route::post('/admin/functions/add', 'FunctionsController@add');
Route::post('/admin/functions/edit', 'FunctionsController@edit');


Route::get('/admin/usuarios', 'UserController@index');
Route::get('/logout', 'UserController@logout');
Route::get('/admin/usuarios/cadastro', 'UserController@cadastro');
Route::get('/admin/usuarios/editar/{id}', 'UserController@editar');


Auth::routes();

Route::get('/home', 'HomeController@index');
